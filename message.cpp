#include<iostream>
#include<map>
#include <string>
#include <utility>
#include <optional>

class Message
{
  std::map < std::string, std::string > _headers;
  std::string _body;

public:
  Message ():_headers (), _body ()
  {}

  Message (const std::map<std::string, std::string> &headers, const std::string &body):
    _headers (headers), 
    _body (body) {}

  Message(const Message &other_message) {
    _headers = other_message._headers;
    _body = other_message._body;
  }

  void add_header(const std::string &header_name, const std::string &header_value)
  {
    _headers.insert({header_name, header_value});
  }

  bool remove_header(const std::string & header_name)
  {
    return _headers.erase(header_name) == 1;
  }

  void split (Message & first_part, Message & second_part)
  {
    size_t body_length = _body.length ();
    std::string first_part_body;
    std::string second_part_body;
    if (body_length > 2) {
      	size_t mid = int (body_length / 2);
      	first_part_body = _body.substr (0, mid);
      	second_part_body = _body.substr (mid, body_length);
    } else {
      	first_part_body = _body;
      	second_part_body = "";
    }
    first_part = Message (_headers, first_part_body);
    second_part = Message (_headers, second_part_body);
  }

  std::string get_body() {
    return _body;
  }

  std::optional<std::string> get_header(const std::string &header_name) {
    auto it = _headers.find(header_name);
    if (it != _headers.end()) {
      return std::optional<std::string>(it->second);
    }
    return std::nullopt;
  }
};


int
main (int argc, char const *argv[])
{
  std::map<std::string, std::string> test_headers ({{"header_1", "value_1"}});
  std::string test_body("Message body");
  Message test_message(test_headers, test_body);
  test_message.add_header("header_2", "value_2");
  test_message.remove_header("header_2");
  test_message.remove_header("header_3");
  Message m_1;
  Message m_2;
  test_message.split(m_1, m_2);
  std::cout << m_1.get_body() << std::endl;
  std::cout << m_2.get_body() << std::endl;
  auto h = m_2.get_header("header_1");
  if (h.has_value()) {
    std::cout << h.value() << std::endl;
  }
  return 0;
}
