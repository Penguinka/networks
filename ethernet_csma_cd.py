from random import randint


if __name__ == "__main__":
    N = int(input())
    tau = 51.2 
    MAX_ITER = 16
    start_time = [0 for i in range(N)]
    current_time = 0
    succeed_stations = []
    count_iterations = [0 for i in range(N)]
    while len(succeed_stations) < N and current_time < 1023:
        now_starting = [(i, t) for i, t in enumerate(start_time) if t == current_time]
        if len(now_starting) > 1:
            # collision
            for i in range(N):
                if start_time[i] == current_time and count_iterations[i] < MAX_ITER:
                    start_time[i] = randint(current_time + 1, min(2 ** (current_time + 1) - 1, 1023)) 
                    count_iterations[i] += 1 
        elif len(now_starting) == 1:
            # no collision
            succeed_stations.append(now_starting[0][0])
            print(now_starting[0][0], ":", round(now_starting[0][1] * tau, 2), "mcs")
        current_time += 1
    if len(succeed_stations) < N:
        print("Some stations did not send package: {}".format([str(i) for i in range(N) if i not in succeed_stations]))
        print("For stations: {} was exceeded limit for retrying".format([str(i) for i in range(N) if count_iterations[i] >= MAX_ITER]))